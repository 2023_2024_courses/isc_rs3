# isc_rs3
```
.devcontainer
   ├── devcontainer.json
   ├── docker-compose.yml
   └── toolchain
       ├── Dockerfile
       └── scripts
           └── get-buildroot.sh
```
```
.vscode
   ├── launch.json
   ├── settings.json
   └── tasks.json
```

```
config
   ├── board
   │   └── friendlyarm
   │       └── nanopi-neo-plus
    ── configs
       └── ses_defconfig
```

```
work
    ├── embedded_systems
    └── secu_it
```