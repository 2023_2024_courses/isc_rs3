//# gcc -o ransomware_encrypt ransomware_encrypt.c -lcrypto

#include <openssl/evp.h>
#include <stdio.h>
#include <string.h>
#include <sys/random.h>


#define BLOCK_SIZE 1024
#define LENGTH_MAX 1200000

static int decrypt (char *pClearFileName, char *pCryptFileName, unsigned char *pIV);

unsigned char key[] = {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
unsigned char iv [] = {3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

int main (void)
{

    decrypt ("encryptedFile", "decryptFile.txt", &iv[0]);
    return (0);
}





static int decrypt   (char *pCryptFileName, char *pDecryptFileName, unsigned char *pIV)
{
FILE          *pCryptFile;
FILE          *pDecryptFile;
unsigned char  outbuf[BLOCK_SIZE];
unsigned char  inbuff[BLOCK_SIZE];
int            nbByteRead;
int            lenCrypt  =0;
int            lenPadding=0;
unsigned int   offset;
unsigned int   nb_block;


unsigned char key1[32];

EVP_CIPHER_CTX *ctx;

    pCryptFile   = fopen(pCryptFileName,   "rb");
    pDecryptFile = fopen(pDecryptFileName, "wb");

    memset (&key1[0],   0, sizeof(key1));
    memset (&inbuff[0], 0, sizeof(inbuff));
    memset (&outbuf[0], 0, sizeof(outbuf));
    fread (&offset, 1, sizeof(offset), pCryptFile);
    fseek(pCryptFile, offset,SEEK_CUR);
    fread (&key1[0], 1, sizeof(key1), pCryptFile);
    fseek(pCryptFile, sizeof(offset),SEEK_SET);

    nb_block = offset / BLOCK_SIZE;



    ctx = EVP_CIPHER_CTX_new();
    EVP_CIPHER_CTX_init (ctx);
    EVP_DecryptInit (ctx, EVP_chacha20 (), &key1[0], pIV);

    for (unsigned int i=0; i < nb_block; i++) {
        nbByteRead=fread(&inbuff[0], 1, sizeof(inbuff), pCryptFile);
        EVP_DecryptUpdate (ctx, outbuf, &lenCrypt, inbuff, nbByteRead);
        fwrite (&outbuf[0], 1, lenCrypt, pDecryptFile);
    }
    fseek(pCryptFile, sizeof(key1),SEEK_CUR);

    for (; (nbByteRead=fread(&inbuff[0], 1, sizeof(inbuff), pCryptFile));) {
        EVP_DecryptUpdate (ctx, outbuf, &lenCrypt, inbuff, nbByteRead);
        fwrite (&outbuf[0], 1, lenCrypt, pDecryptFile);
    }

    EVP_DecryptFinal (ctx, &outbuf[lenCrypt], &lenPadding);
    fwrite (&outbuf[lenCrypt], 1, lenPadding, pDecryptFile);

    EVP_CIPHER_CTX_cleanup (ctx);
    EVP_CIPHER_CTX_free(ctx);
    fclose (pDecryptFile);
    fclose (pCryptFile);
    return (0);
}



