// gcc -Wall password_1.c -o password_1

#include <string.h>
#include <stdio.h>

#define TEST0 "password1"
#define TEST1 "+*%&"
#define TEST2 "edr45"


int main (void)
{
unsigned char  buffer1[32];
unsigned char  buffer2[8];
int   flag;
char *p;


    p=TEST0;
    memset (&buffer1[0], 0, sizeof(buffer1));
    p=TEST1;
    printf ("Enter password=");
    p=TEST2;
    scanf ("%s", &buffer1[0]);

    buffer2[0] = buffer1[0] + buffer1[1] - buffer1[2];
    buffer2[1] = buffer1[2] ^ buffer1[3];
    buffer2[2] = buffer1[4] & buffer1[5];

    flag=0;
    if (buffer2[0] != 'a')
        flag = 1;
    if (buffer2[1] != 0)
        flag = 1;
    if (buffer2[2] != '0')
        flag = 1;
    

    if (flag == 0)
        printf ("Good password\n");         //aaaa00, bcdd01, cdff01, cdff02, ..., cdff0?
    else
        printf ("Invalid password\n");

    return 0;
}
