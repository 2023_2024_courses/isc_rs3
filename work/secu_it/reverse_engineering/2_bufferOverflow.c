//gcc -Wall -Wextra -fno-stack-protector -o 2_bufferOverflow 2_bufferOverflow.c

#include <stdio.h>
#define BUF_SIZE 10

int main(){
    unsigned int buf[BUF_SIZE];
    unsigned int i = 0;

    //printf("%zu", sizeof(buf));


    while (i < sizeof(buf)) {
        buf[i] = 1;
        printf("%d",buf[i]);
        i++;
    }
    
    printf("Buf value after the read: \n");
    return (0);
}
