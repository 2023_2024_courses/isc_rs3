//# gcc -o ransomware_encrypt ransomware_encrypt.c -lcrypto
// pour 2024, l'indication de l'offset est en 0, la cle est random et se trouve en 0x400 + 4

#include <openssl/evp.h>
#include <stdio.h>
#include <string.h>
#include <sys/random.h>


#define BLOCK_SIZE 1024
#define LENGTH_MAX 1200000

static int encrypt (char *pClearFileName, char *pCryptFileName, unsigned char *pIV);

unsigned char key[] = {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
unsigned char iv [] = {3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

int main (void)
{

    encrypt ("clearFile.txt", "encryptedFile",   &iv[0]);
    return (0);
}



static int encrypt   (char *pClearFileName, char *pCryptFileName, unsigned char *pIV)
{
FILE          *pClearFile;
FILE          *pCryptFile;
unsigned char  outbuf[BLOCK_SIZE];
unsigned char  inbuff[BLOCK_SIZE];
int            nbByteRead;
int            lenCrypt  =0;
int            lenPadding=0;
unsigned int   offset;
unsigned int   nb_block;
unsigned int  *pkey;

unsigned char key1[32];

EVP_CIPHER_CTX *ctx;

    getrandom (&key1[0], sizeof(key1), 0);
    //memcpy (&key1[0],&key[0],sizeof(key1));

    ctx = EVP_CIPHER_CTX_new();
    EVP_CIPHER_CTX_init (ctx);
    EVP_EncryptInit (ctx, EVP_chacha20 (), &key1[0], pIV);

        


    pClearFile = fopen(pClearFileName, "rb");
    pCryptFile = fopen(pCryptFileName, "wb");
    pkey = (unsigned int*)&key1[0];
    //offset = (*pkey) % LENGTH_MAX;    // plus difficile
    //offset = 1199999;
    offset = 1024;                      // plus facile, la cle est en 0x400 +4

    nb_block = offset / BLOCK_SIZE;
    offset = nb_block * BLOCK_SIZE;


    fwrite (&offset, 1, sizeof(offset), pCryptFile);

    memset (&inbuff[0], 0, sizeof(inbuff));
    memset (&outbuf[0], 0, sizeof(outbuf));

    for (unsigned int i=0; i < nb_block; i++) {
        nbByteRead=fread(&inbuff[0], 1, sizeof(inbuff), pClearFile);
        EVP_EncryptUpdate (ctx, outbuf, &lenCrypt, inbuff, nbByteRead);
        fwrite (&outbuf[0], 1, lenCrypt, pCryptFile);
    }

    fwrite (&key1[0], 1, sizeof(key1), pCryptFile);

    for (; (nbByteRead=fread(&inbuff[0], 1, sizeof(inbuff), pClearFile));) {
        EVP_EncryptUpdate (ctx, outbuf, &lenCrypt, inbuff, nbByteRead);
        fwrite (&outbuf[0], 1, lenCrypt, pCryptFile);
    }

    EVP_EncryptFinal (ctx, &outbuf[lenCrypt], &lenPadding);
    fwrite (&outbuf[lenCrypt], 1, lenPadding, pCryptFile);
    

    EVP_CIPHER_CTX_cleanup (ctx);
    EVP_CIPHER_CTX_free(ctx);
    fclose (pClearFile);
    fclose (pCryptFile);
    return (0);
}

