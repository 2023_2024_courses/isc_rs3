# Closest Vector Problem 1
import numpy as np

A = np.array([[5, -2], 
              [1, 8]])
y = np.array([[27], 
              [8]])
x1 = np.array([[6], 
              [0]])

# Compute x: y=Ax
A_inv = np.linalg.inv(A)
x=A_inv.dot(y)
print("x=",x)

# Compute y1: y1=Ax1
print ("y=", y)
print ("y1=", A.dot(x1))


y
#print (y1)
