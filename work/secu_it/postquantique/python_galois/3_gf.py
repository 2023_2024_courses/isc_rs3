import galois
import numpy as np

gf = galois.GF(31**2, repr="poly")
ir = np.array(galois.Poly ([1,0,1], field=gf))

print(gf.elements)
print(gf.properties)
print(gf.characteristic)

# Alice public: t = ((As + e) mod q) mod (x2+1)
A = np.array([[galois.Poly([12,30], field=gf), galois.Poly([27,29], field=gf)], 
              [galois.Poly([28,14], field=gf), galois.Poly([14,15], field=gf)]])

s = np.array([[galois.Poly([2,2], field=gf)], 
              [galois.Poly([1,1], field=gf)]])
e = np.array([[galois.Poly([1,0], field=gf)], 
              [galois.Poly([2,1], field=gf)]])

t= (A.dot(s) % ir) + e
print (t)
