# cd /usr/local/lib/python3.10/dist-packages/galois
# grep -r "def GF" *
#    _fields/_factory.py:def GF(


import galois

gf=galois.GF(7)
print (gf.elements)
print(gf.arithmetic_table("+"))
print(gf.arithmetic_table("-"))
print(gf.arithmetic_table("*"))
print(gf.arithmetic_table("/"))

a=gf(3)
b=gf(6)
print (a+b)
print (a-b)
print (a*b)
print (a/b)

