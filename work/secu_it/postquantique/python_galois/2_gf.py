# cd /usr/local/lib/python3.10/dist-packages/galois
# grep -r "def GF" *
#    _fields/_factory.py:def GF(


import galois
gf = galois.GF(3**3, repr="int")
#gf = galois.GF(3**2, repr="poly")
print (gf.elements)
print(gf.arithmetic_table("+"))

a = galois.Poly([1,2], field=gf)  # a=12 = 1x + 2
b = galois.Poly([1,1], field=gf)  # b=11 = 1x + 1
print (a+b)
c = galois.Poly([1,0,1], field=gf)
print (a%c)
