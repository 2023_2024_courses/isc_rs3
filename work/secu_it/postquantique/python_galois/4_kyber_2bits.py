import galois
import numpy as np

gf = galois.GF(31**2, repr="poly")
ir = np.array(galois.Poly ([1,0,1], field=gf))

print(gf.elements)
print(gf.properties)
print(gf.characteristic)

# Alice
# public A and t
# Alice private: s
# Alice public: t = ((As + e) mod q) mod (x2+1)
A = np.array([[galois.Poly([12,30], field=gf), galois.Poly([27,29], field=gf)], 
              [galois.Poly([28,14], field=gf), galois.Poly([14,15], field=gf)]])

s = np.array([[galois.Poly([2,2], field=gf)], 
              [galois.Poly([1,1], field=gf)]])
e = np.array([[galois.Poly([1,0], field=gf)], 
              [galois.Poly([2,1], field=gf)]])

t= (A.dot(s) % ir) + e
print (t)



# Bob
# public A and t
# Bob private: r

r = np.array([[galois.Poly([2,1], field=gf)], 
              [galois.Poly([1,1], field=gf)]])
e1 = np.array([[galois.Poly([1,0], field=gf)], 
              [galois.Poly([1,1], field=gf)]])
e2 = np.array([galois.Poly([0,1], field=gf)])
m = np.array([galois.Poly([15,15], field=gf)])
 
u = np.dot(np.transpose(r),A) % ir + np.transpose(e1)
v = np.dot(np.transpose(r),t) % ir + e2 + m
     

print (v)    

# Decryption
m_noise = (v - np.dot(u,s))%ir
print (m_noise)


