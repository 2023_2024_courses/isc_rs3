import numpy as np

q=31
# Alice
# public A and t
# Alice private: s
# Alice public: t = (Ax + e) mod q
A = np.array([[28, 13], 
              [28, 25]])
s = np.array([[-1], 
              [-1]])
e = np.array([[1],
              [1]])
t=(A.dot(s) + e) % q
print ("t=", t)

# Bob
# public A and t
# Bob private: r
A = np.array([[28, 13], 
              [28, 25]])
r = np.array([[-1], 
              [1]])
e1 = np.array([[1, 1]])
e2 = np.array([[-1]])
m=1
if (m==1):
    m=q//2
else:
    m=0    
u = (np.dot(np.transpose(r),A) + e1) % q
v = (np.dot(np.transpose(r),t) + e2 + m) % q

print ("u=", u)
print ("v=", v)


# Decryption
m_noise = (v - np.dot(u,s))%q
print ("m+noise", m_noise)
if ((m_noise <= 3*q//4) and (m_noise > q//4)):
    m = 1
else:
    m = 0
print ("m = ", m)




'''
x=np.random.randint(3, size=(n,1))
u=np.dot(A,x)

# Bob private: s, e1, e2
# Bob public: b1, b2: b1=As + e1
#                   : b2=su + e2 + bit*q/2 
s=np.random.randint(3, size=(n,1))
e1=np.random.randint(3, size=(n,1)) -1
e2=np.random.randint(1, size=(1,1)) -1

# Encryption
b1 = (np.dot(A,s) + e1) % q
#b2 = (np.dot(np.transpose(s),u) + e2 + q/2) % q
b2 = (np.dot(np.transpose(s),u) + e2) % q


# Decryption
val = (b2 - np.dot(np.transpose(b1), x))%q

print (val)

#print (b1)



#b1 = b1%q
#print (b1)

#b2 = s





#print (A)
#print ("Alice private:", x)
#print ("Alice public:", u)
#print ("Bob private s:", s)
#print ("Bob private e1:", e1)
#print ("Bob public:", b1)

#print (u)
'''