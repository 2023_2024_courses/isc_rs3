//# gcc -Wall -Wextra -o base64 base64.c -lcrypto

	
#include <openssl/evp.h>
#include <stdio.h>
#include <string.h>


static int printHex  (char *pText, unsigned char *p, int length);
static int blockToken(void);
static int token     (void);



static unsigned char token1[]      = {0x23,0x45,0x11,0x56,0x89,0x39};
static unsigned char token2          [16];
static char          token1_B64      [16];

static unsigned char blockToken1[] = {0x23,0x45,0x11,0x56,0x89,0x39};
static unsigned char blockToken2    [16];
static char          blockToken1_B64[16];




int main ()
{


    blockToken();  //encode +decode par bloc de 6 bytes --> pas de padding
    token();       //encode +decode par bloc autre que 6 bytes --> padding


    return (0);
}



static int blockToken(void){
int nbByte;

    memset (&blockToken2[0],     0, sizeof(blockToken2));
    memset (&blockToken1_B64[0], 0, sizeof(blockToken1_B64));

    nbByte=EVP_EncodeBlock((unsigned char*)&blockToken1_B64[0], &blockToken1[0], sizeof(blockToken1));
    printf("blockToken1_b64=%s\n", &blockToken1_B64[0]);
    printHex("blockToken1_b64 hex=", &blockToken1_B64[0], nbByte);

    nbByte=EVP_DecodeBlock((unsigned char*)&blockToken2[0], &blockToken1_B64[0], nbByte);
    printHex("blockToken1 hex=", &blockToken1[0], sizeof(blockToken1));
    printHex("blockToken2 hex=", &blockToken2[0], nbByte);
    return (0);
    
}


static int token(void){
int nbByte;
int len;
int i;
EVP_ENCODE_CTX *ctx;

    memset (&token2[0],     0, sizeof(token2));
    memset (&token1_B64[0], 0, sizeof(token1_B64));
    
    ctx = EVP_ENCODE_CTX_new();
    EVP_EncodeInit(ctx);
    i=0;
    EVP_EncodeUpdate(ctx, &token1_B64[i], &len, &token1[0], 2);
    i = i+len;
    EVP_EncodeUpdate(ctx, &token1_B64[i], &len, &token1[2], 2);
    i = i+len;
    EVP_EncodeUpdate(ctx, &token1_B64[i], &len, &token1[4], 1);
    i = i+len;
    EVP_EncodeUpdate(ctx, &token1_B64[i], &len, &token1[5], 1);
    i = i+len;
    EVP_EncodeFinal (ctx, &token1_B64[i], &len);
    EVP_ENCODE_CTX_free(ctx);

    printf("token1_b64=%s\n", &token1_B64[0]);
    printHex("token1_b64 hex=", &token1_B64[0], strlen(token1_B64));



    // strlen(token1_B64) == 9
    ctx = EVP_ENCODE_CTX_new();
    EVP_DecodeInit(ctx);
    i=0;
    EVP_DecodeUpdate(ctx, &token2[i], &len, &token1_B64[0], 4);
    i = i+len;
    EVP_DecodeUpdate(ctx, &token2[i], &len, &token1_B64[4], 2);
    i = i+len;
    EVP_DecodeUpdate(ctx, &token2[i], &len, &token1_B64[6], 3);
    i = i+len;
    EVP_DecodeFinal (ctx, &token2[i], &len);
    i = i+len;
    EVP_ENCODE_CTX_free(ctx);
    printHex("token1 hex=", &token1[0], sizeof(token1));
    printHex("token2 hex=", &token2[0], i);



    /*
    nbByte=EVP_DecodeBlock((unsigned char*)&blockToken2[0], &blockToken1_B64[0], nbByte);
    printHex("blockToken1 hex=", &blockToken1[0], sizeof(blockToken1));
    printHex("blockToken2 hex=", &blockToken2[0], nbByte);
    */
   return (0);
    
}




    
    
static int printHex (char *pText, unsigned char *p, int length)
{
int i;
    printf ("%s 0x", pText);
    for (i=0; i<length; i++)
        printf ("%.2x", *(p+i));
    printf ("\n");
    return 0;
}
  

