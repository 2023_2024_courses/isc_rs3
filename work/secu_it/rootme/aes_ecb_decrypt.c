#include <openssl/evp.h>
#include <stdio.h>
#include <string.h>

#define BUF_SIZE 128
#define KEY_SIZE  20

static int decrypt (char *pEncryptedFileName, char *pDecryptedFileName);


int main (int argc, char *argv[])
{
    decrypt ("mylogin.cnf", "password");
    return (0);
}

int decrypt (char *pEncryptedFileName, char *pDecryptedFileName) {
FILE          *pEncryptedFile;
FILE          *pDecryptedFile;
unsigned char tmp[4];
unsigned char key[20];
unsigned char rkey[16];
unsigned char iv[16];
unsigned char encryptedPassword[96];
unsigned char clearPassword[96];
int           nbByteRead;
int           lenCrypt;
int           lenPadding;
EVP_CIPHER_CTX *ctx;

    memset (&tmp[0], 0, sizeof(tmp));
    memset (&key[0], 0, sizeof(key));
    memset (&rkey[0], 0, sizeof(rkey));
    memset (&iv[0], 0, sizeof(iv));
    memset (&encryptedPassword[0], 0, sizeof(encryptedPassword));
    memset (&clearPassword[0], 0, sizeof(clearPassword));



    pEncryptedFile   = fopen(pEncryptedFileName,   "rb");
    pDecryptedFile   = fopen(pDecryptedFileName,   "wb");

    nbByteRead=(int)fread(&tmp[0], 1, sizeof(tmp), pEncryptedFile);  // Read 4 bytes
    nbByteRead=(int)fread(&key[0], 1, sizeof(key), pEncryptedFile);  // Read key, 20 bytes
    //Compute real key
    for (int i= 0; i < 20; ++i)
        rkey[i%16] ^= key[i];




    nbByteRead=(int)fread(&tmp[0], 1, sizeof(tmp), pEncryptedFile);  

    nbByteRead=(int)fread(&encryptedPassword [0], 1, 16, pEncryptedFile);
    nbByteRead=(int)fread(&tmp[0], 1, sizeof(tmp), pEncryptedFile);  
    nbByteRead=(int)fread(&encryptedPassword[16], 1, 16, pEncryptedFile);
    nbByteRead=(int)fread(&tmp[0], 1, sizeof(tmp), pEncryptedFile); 
    nbByteRead=(int)fread(&encryptedPassword[32], 1, 32, pEncryptedFile);
    nbByteRead=(int)fread(&tmp[0], 1, sizeof(tmp), pEncryptedFile); 
    nbByteRead=(int)fread(&encryptedPassword[64], 1, 32, pEncryptedFile);
    

    ctx = EVP_CIPHER_CTX_new();
    EVP_CIPHER_CTX_init (ctx);
    EVP_DecryptInit (ctx, EVP_aes_128_ecb (), &rkey[0], &iv[0]);
    EVP_DecryptUpdate (ctx, &clearPassword[0], &lenCrypt, &encryptedPassword[0], 96);
    fwrite (&clearPassword[0], 1, lenCrypt, pDecryptedFile);
    EVP_DecryptFinal (ctx, &clearPassword[lenCrypt], &lenPadding);
    fwrite (&clearPassword[lenCrypt], 1, lenPadding, pDecryptedFile);





    fclose (pEncryptedFile);
    fclose (pDecryptedFile);


}
