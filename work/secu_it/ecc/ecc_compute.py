# extended Euclidean Algorithm: egcd
# e*d%(p-1)(q-1) = 1
# e is fixed and must be relative prime with (p-1)(q-1) -> gcd ((p-1)(q-1), e) = 1
# this equation e*d%(p-1)(q-1) = 1 can be solved with the egcd

# g = gcd(a, b) = ax + by: g must be 1: a = (p-1)(q-1): b = e
# (g, x, y) = egcd(a, b)
# e is fixed, d = g%(p-1)(q-1)

'''
def add_base       (self, p1, p2)
def add            (self, primitive_point, nb_addition)
def add_print      (self, primitive_point, nb_addition)
def double_and_add (self, primitive_point, private_key)
'''


from curve_definition import *



def egcd(a, b):
    if b == 0:
        return (a, 1, 0)
    else:
        g, x, y = egcd(b, a % b)
        #print (a, b, (a//b), g, x, y)
        return (g, y, x - (a // b) * y)




class ecc_curve(object):
    def __init__(self, a, b, p, order):
        self.a = a
        self.b = b
        self.p = p
        self.s = 0
        self.primitive_point = Point(0,0)
        self.order = order

    # return (p1 + p2)        
    def add_base (self, p1, p2):
        #if (self.primitive_point.x == p2.x) and (self.primitive_point.y == -p2.y):   # detect the infinity point
        if ((self.primitive_point.x == p2.x) and (self.primitive_point.y == -p2.y)) or ((p1.x == p2.x) and (p1.y == (-p2.y%self.p))):   # detect the infinity point
            infinity_point = True
            x3 = 0
            y3 = 0
        else:                                    # normal points        
            self.compute_s (p1,p2)
            x3 = (self.s ** 2 - p1.x - p2.x)%self.p
            y3 = (self.s * (p1.x - x3) - p1.y)%self.p
            # Detect -P
            if x3 == self.primitive_point.x:
                if (-self.primitive_point.y % self.p == y3):
                    #y3 = -p1.y
                    y3 = -self.primitive_point.y
            infinity_point = False
        return (infinity_point, Point(x3, y3))


    def compute_s (self, p1, p2):
        if p1 != p2:
            ## be careful: if p2.x < p1.x --> negative number  
            if p2.x > p1.x:     # denominator is positif
                g,x,y=egcd(self.p, p2.x-p1.x)
                d=y%self.p
                self.s =((p2.y-p1.y) * d)%self.p
            else:               # denominator is negatif
                g,x,y=egcd(self.p, p1.x-p2.x)
                d=y%self.p
                self.s =((p1.y-p2.y) * d)%self.p

        else:
            g,x,y=egcd(self.p, 2*p1.y)
            d=y%self.p
            self.s=((3*(p1.x**2) + self.a) * d)%self.p

    # return (nb_addition * primitive_point: primitive_point + primitive_point + ...)        
    def add (self, primitive_point, nb_addition):
        self.primitive_point = primitive_point
        p1=primitive_point
        p2 = p1
        #print ("P   = ", p1.x, ", ", p1.y)
        for i in range (0, nb_addition):
            infinity_point = False
            infinity_point, p2 = self.add_base(p1, p2)
            if (infinity_point == True): #False):
                p2 = p1
                #print (i+2,"P = ", p2.x, ", ", p2.y)
            #else:
                #print (i+2, "P = infinity point")
        return (p2)
    
    def add_print (self, primitive_point, nb_addition):
        self.primitive_point = primitive_point
        p1=primitive_point
        p2 = p1
        print ("P   = ", p1.x, ", ", p1.y)
        for i in range (0, nb_addition):
            infinity_point = False
            infinity_point, p2 = ecc.add_base(p1, p2)
            if (infinity_point == False):
                print (i+2,"P = ", p2.x, ", ", p2.y)
            else:
                p2 = p1
                print (i+2, "P = infinity point")
        return (p2)

    def double_and_add_base (self, p2, p1):
        infinity_point = False
        infinity_point, p2 = self.add_base(p2, p1)
        if (infinity_point == True):
            p2 = p1
        return (p2)

    # return (private_key * primitive_point: primitive_point + primitive_point + ...)        
    def double_and_add (self, primitive_point, private_key):
        if private_key < self.order:
            self.primitive_point.x = primitive_point.x
            self.primitive_point.y = primitive_point.y
                    
            nb_bits = 0
            mask = 0x1
     
            # find number of bits for privte_key
            while mask <= private_key:
                nb_bits = nb_bits + 1
                mask = mask << 1
    
            nb_bits -= 1
            mask >>= 2
    
            p2=primitive_point
            # compute public key with doubling and add algorithm
            while nb_bits > 0:
                p2=self.double_and_add_base (p2,p2)
                if (private_key & mask) != 0:
                    p2=self.double_and_add_base (p2,primitive_point)

                nb_bits -= 1
                mask >>=1
            return (p2)
        else:
            print ("private key > curve order =", self.order)
            p2=primitive_point
            
            return (p2)



class Point(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __eq__(self, other):
        if self.x == other.x and self.y == other.y:
            return True
        else:
            return False



# ecc.add_base()
curve_name = "paarbook1"
primitive_point=Point(ECC_CURVES[curve_name]["g"][0],ECC_CURVES[curve_name]["g"][1])
ecc = ecc_curve (a=ECC_CURVES[curve_name]["a"], b=ECC_CURVES[curve_name]["b"], p=ECC_CURVES[curve_name]["p"], order=ECC_CURVES[curve_name]["n"])

p1 = Point (5,1)
p2 = Point (5,1)
infinity_point, p = ecc.add_base(p1, p2)
print ("========  ecc.add_base =========")
print ("infinity_point=", infinity_point, "x=", p.x, "y=",p.y)

# ecc.add_print()
curve_name = "paarbook1"
primitive_point=Point(ECC_CURVES[curve_name]["g"][0],ECC_CURVES[curve_name]["g"][1])
ecc = ecc_curve (a=ECC_CURVES[curve_name]["a"], b=ECC_CURVES[curve_name]["b"], p=ECC_CURVES[curve_name]["p"], order=ECC_CURVES[curve_name]["n"])
p=ecc.add_print (primitive_point,2)
print ("========= ecc.add_base =========")
print ("x=", p.x, "y=",p.y)


# ecc.doouble_and_add()
curve_name = "paarbook1"
primitive_point=Point(ECC_CURVES[curve_name]["g"][0],ECC_CURVES[curve_name]["g"][1])
private_key = 2
public_key = ecc.double_and_add(primitive_point, private_key)
print ("========= ecc.double_and_add =========")
print ("private key = ", private_key, "public key =", public_key.x, public_key.y)


'''
p = ecc.add(primitive_point, 2)
print ("x=", p.x, "y=",p.y)
'''

'''
#primitive_point=Point(8,10)
p=ecc.add_print (primitive_point,20)
print ("x=", p.x, "y=",p.y)
'''


'''
private_key = 2
public_key = ecc.double_and_add(primitive_point, private_key)
print ("private key = ", private_key, "public key =", public_key.x, public_key.y)
'''








"""
curve_name = "paarbook_prob_95"
primitive_point=Point(ECC_CURVES[curve_name]["g"][0],ECC_CURVES[curve_name]["g"][1])
ecc = ecc_curve (a=ECC_CURVES[curve_name]["a"], b=ECC_CURVES[curve_name]["b"], p=ECC_CURVES[curve_name]["p"], order=ECC_CURVES[curve_name]["n"])
primitive_point= Point(0,3)
p=ecc.add_print (primitive_point,20)
"""





"""
curve_name = "secp256r1"
primitive_point=Point(ECC_CURVES[curve_name]["g"][0],ECC_CURVES[curve_name]["g"][1])
ecc = ecc_curve (a=ECC_CURVES[curve_name]["a"], b=ECC_CURVES[curve_name]["b"], p=ECC_CURVES[curve_name]["p"], order=ECC_CURVES[curve_name]["n"])

nb_addition = 100000
p=ecc.add (primitive_point,nb_addition)
print (p.x, p.y)



#private_key=100001
private_key = 0xffffffff00000000ffffffffffffffffbce6faada7179e84f3b9cac2fc632540
public_key = ecc.double_and_add(primitive_point, private_key=private_key)
print ("private key = ", private_key, "public key =", public_key.x, public_key.y)

"""


"""
curve_name = "paarbook1"
primitive_point=Point(ECC_CURVES[curve_name]["g"][0],ECC_CURVES[curve_name]["g"][1])
#primitive_point=Point(10,6)
"""

"""
ecc = ecc_curve (a=ECC_CURVES[curve_name]["a"], b=ECC_CURVES[curve_name]["b"], p=ECC_CURVES[curve_name]["p"], order=ECC_CURVES[curve_name]["n"])
#ecc.add (primitive_point,nb_addition=20)
private_key=11
public_key = ecc.double_and_add(primitive_point, private_key=private_key)
print ("private key = ", private_key, "public key =", public_key.x, public_key.y)
"""

#p1=Point(5,1)
#ecc = ecc_curve (a=0, b=7, p=17)

