# openssl genrsa -3 -out 1_rsa_compute_key.pem 70
# or
# openssl genrsa -3 -out 1_rsa_compute_key.pem 1024
# openssl pkey -in 1_rsa_compute_key.pem -text

p1 = '''
    00:e2:f3:48:19:51:61:4c:17:3a:2f:ea:49:fd:c2:
    fc:70:72:a0:ef:a1:3d:fd:4c:fb:2f:eb:21:ad:6d:
    b9:ec:d9:9b:09:6d:94:77:29:28:ef:0f:5f:a1:c5:
    70:45:98:9b:14:da:4e:54:d2:e9:57:f3:09:c9:5c:
    1d:e4:21:be:e1
'''
q1 = '''
    00:da:6c:1a:6c:75:26:53:9d:0a:84:c6:d9:f7:8f:
    71:5f:36:7a:45:56:96:dd:a5:48:0a:b3:5d:58:95:
    af:85:c0:f4:e2:fb:e9:33:99:69:d3:80:b5:0c:76:
    d0:6e:ba:f7:c2:cd:37:3d:23:d3:0d:46:32:64:ee:
    9f:f3:27:54:c7
'''




def egcd(a, b):
    if b == 0:
        return (a, 1, 0)
    else:
        g, x, y = egcd(b, a % b)
        #print g, x, y
        return (g, y, x - (a // b) * y)

# change : to empty, \n to empty, ' ' to empty 
maps = {':': '', '\n':'',' ':''}
table=str.maketrans(maps)
p1=p1.translate(table)
q1=q1.translate(table)

p=int(p1,16)
q=int(q1,16)
n=p*q
phi = (p-1)*(q-1)
e=0x10001
g,x,y=egcd(phi, e)
if (g==1):
    d=y%phi
    print ("p=", hex(p), "q=", hex(q))
    print ("n=", hex(n), "phi=", hex(phi))
    print ("Public key=", hex(e), hex(n))
    print ("Private key=", hex(d), hex(n))
else:
    print ("e and phi are not relatively prime")
    print ("e=", hex(e), "phi=", hex(phi))



#m=0x02
#enc = (m ** e)%n
#m1 = (enc ** d)%n
#print enc
#print m1


#g,x,y=egcd (40,7)

#d=y%40
#print y
#print d
