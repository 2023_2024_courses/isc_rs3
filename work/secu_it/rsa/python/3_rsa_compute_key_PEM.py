#https://bitbucket.org/sybren/python-rsa/src/01487233708ecd4ddb3063fc36dab658280e469a/rsa/key.py?at=default

# pip install pyasn1

# extended Euclidean Algorithm: egcd
# e*d%(p-1)(q-1) = 1
# e is fixed and must be relative prime with (p-1)(q-1) -> gcd ((p-1)(q-1), e) = 1
# this equation e*d%(p-1)(q-1) = 1 can be solved with the egcd

# g = gcd(a, b) = ax + by: g must be 1: a = (p-1)(q-1): b = e
# (g, x, y) = egcd(a, b)
# e is fixed, d = g%(p-1)(q-1)

from pyasn1.type import univ, namedtype
from pyasn1.codec.der import encoder
import base64



def save_pkcs1_pem(n, d, e, p, q, exp1, exp2, coef):
    '''Saves the private key in PKCS#1 DER format.
    '''

    class AsnPrivKey(univ.Sequence):
        componentType = namedtype.NamedTypes(
            namedtype.NamedType('version', univ.Integer()),
            namedtype.NamedType('modulus', univ.Integer()),
            namedtype.NamedType('publicExponent', univ.Integer()),
            namedtype.NamedType('privateExponent', univ.Integer()),
            namedtype.NamedType('prime1', univ.Integer()),
            namedtype.NamedType('prime2', univ.Integer()),
            namedtype.NamedType('exponent1', univ.Integer()),
            namedtype.NamedType('exponent2', univ.Integer()),
            namedtype.NamedType('coefficient', univ.Integer()),
        )

    # Create the ASN object
    asn_key = AsnPrivKey()
    asn_key.setComponentByName('version', 0)
    asn_key.setComponentByName('modulus', n)
    asn_key.setComponentByName('publicExponent', e)
    asn_key.setComponentByName('privateExponent', d)
    asn_key.setComponentByName('prime1', p)
    asn_key.setComponentByName('prime2', q)
    asn_key.setComponentByName('exponent1', exp1)
    asn_key.setComponentByName('exponent2', exp2)
    asn_key.setComponentByName('coefficient', coef)

    b64_byte = base64.encodebytes(encoder.encode(asn_key))
    b64 = b64_byte.decode('ascii')

    asn_key_pem = "-----BEGIN RSA PRIVATE KEY-----\n%s-----END RSA PRIVATE KEY-----" % b64

    return asn_key_pem


def egcd(a, b):
    if b == 0:
        return (a, 1, 0)
    else:
        g, x, y = egcd(b, a % b)
        #print a, b, (a//b), g, x, y
        return (g, y, x - (a // b) * y)


#http://b3ck.blogspot.ch/2011/06/how-to-break-rsa-explicitly-with.html
p=15874691959611300883
q=18002140047319421731

n=p*q
phi = (p-1)*(q-1)
e=0x10001

g,x,y=egcd(phi, e)
if (g==1):
    d=y%phi
    pem=save_pkcs1_pem(n, d, e, p, q, d%(p-1), d%(q-1), 0)
    #pem=save_pkcs1_pem(n, d, e, p, q, 10736473428822113971, 4759465867677384223, 2980629121260951946)

    #coefficient       INTEGER,  -- (inverse of q) mod p

    #print hex(n), hex(d), hex(e), hex(p), hex(q), hex(d%(p-1)), hex(d%(q-1))
    print (pem)

else:
    print ("e and phi are not relatively prime")
    print ("e=", e, "phi=", phi)




