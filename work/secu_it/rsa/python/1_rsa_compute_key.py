# extended Euclidean Algorithm: egcd
# e*d%(p-1)(q-1) = 1
# e is fixed and must be relative prime with (p-1)(q-1) -> gcd ((p-1)(q-1), e) = 1
# this equation e*d%(p-1)(q-1) = 1 can be solved with the egcd

# g = gcd(a, b) = ax + by: g must be 1: a = (p-1)(q-1): b = e
# (g, x, y) = egcd(a, b)
# e is fixed, d = g%(p-1)(q-1)

def egcd(a, b):
    if b == 0:
        return (a, 1, 0)
    else:
        g, x, y = egcd(b, a % b)
        print (a, b, (a//b), g, x, y)
        return (g, y, x - (a // b) * y)


#http://b3ck.blogspot.ch/2011/06/how-to-break-rsa-explicitly-with.html
#p=18323751683953135859
#q=18168020807219392517
#p=23
#q=29
#p=64817
#q=64499
p=223
q=221


#e=29
#e=65537
e=821

n=p*q
phi = (p-1)*(q-1)

g,x,y=egcd(phi, e)
if (g==1):
    d=y%phi
    print ("p=", p, ",", hex(p), "q=", q, ",", hex(q))
    print ("n=", n, ",", hex(n), "phi=", phi, ",", hex(phi))
    print ("Public key=", e, ",", hex(e), n, ",", hex(n)) 
    print ("Private key=", d, ",", hex(d), n, ",", hex(n))
else:
    print ("e and phi are not relatively prime")
    print ("e=", e, "phi=", phi)



#c1=0xaa1f
#m1=(c1**d)%n
#print ("ccccc decrypt=",hex(m1))

#c1=0x7547
#m1=(c1**d)%n
#print ("cccc decrypt=",hex(m1))

c1=0x1faa
m1=(c1**d)%n
print ("ccccc decrypt=",hex(m1))

c1=0x4775
m1=(c1**d)%n
print ("cccc decrypt=",hex(m1))



m=0x7765
c = (m**e)%n
print ("encrypt=",hex(c))
m1=(c**d)%n
print ("decrypt=",hex(m1))

m=0x656b
c = (m**e)%n
print ("encrypt=",hex(c))

m1=(c**d)%n
print ("decrypt=",hex(m1))


