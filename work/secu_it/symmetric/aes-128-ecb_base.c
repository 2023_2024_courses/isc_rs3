//# gcc -Wall -Wextra -o aes-128-ecb_base aes-128-ecb_base.c -lcrypto

	
//#include <openssl/blowfish.h>
#include <openssl/evp.h>
#include <stdio.h>
#include <string.h>

#define BUF_SIZE  128

static int saveClearText_to_File(unsigned char *pClearText, int length, char *pFileClearTextName);
static int encrypt   (char *pClearFileName, char *pCryptFileName, unsigned char *pKey, unsigned char *pIV);
static int decrypt   (char *pClearFileName, char *pCryptFileName, unsigned char *pKey, unsigned char *pIV);

// Clear text with 32 bytes
unsigned char clearText[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31};


unsigned char key[] = {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
unsigned char iv [] = {3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

int main ()
{

    saveClearText_to_File(&clearText[0], sizeof(clearText), "clearFile");
    encrypt   ("clearFile", "cryptFile",   &key[0], &iv[0]);
    decrypt ("cryptFile", "decryptFile", &key[0], &iv[0]);
    return (0);
}


static int saveClearText_to_File(unsigned char *pClearText, int length, char *pFileClearTextName)
{
FILE          *pClearFile;
int            nbByteRead;

    pClearFile = fopen (pFileClearTextName, "wb");
    nbByteRead = fwrite(pClearText, 1, length, pClearFile);
    fclose(pClearFile);
    return (0);
}


static int encrypt   (char *pClearFileName, char *pCryptFileName, unsigned char *pKey, unsigned char *pIV)
{
FILE          *pClearFile;
FILE          *pCryptFile;
unsigned char  cryptBuf[BUF_SIZE];
unsigned char  clearBuf[BUF_SIZE];
int            nbByteRead;
int            lenCrypt  =0;
int            lenPadding = 0;



//EVP_CIPHER_CTX ctx;
EVP_CIPHER_CTX *ctx;

    memset (&clearBuf[0], 0, sizeof(clearBuf));
    memset (&cryptBuf[0], 0, sizeof(cryptBuf));


    pClearFile = fopen(pClearFileName, "rb");
    pCryptFile = fopen(pCryptFileName, "wb");
    nbByteRead = fread(&clearBuf[0], 1, sizeof(clearBuf), pClearFile);
    
    
    ctx = EVP_CIPHER_CTX_new();
    EVP_CIPHER_CTX_init (ctx);
    EVP_EncryptInit     (ctx, EVP_aes_128_ecb(), pKey, pIV);

    EVP_EncryptUpdate   (ctx, &cryptBuf[0], &lenCrypt, &clearBuf[0], nbByteRead);
    fwrite (&cryptBuf[0], 1, nbByteRead, pCryptFile);
    EVP_EncryptFinal    (ctx, &cryptBuf[lenCrypt], &lenPadding);
    fwrite (&cryptBuf[lenCrypt], 1, lenPadding, pCryptFile);



    EVP_CIPHER_CTX_cleanup (ctx);
    EVP_CIPHER_CTX_free(ctx);
    
    fclose (pClearFile);
    fclose (pCryptFile);
    return (0);
}


static int decrypt   (char *pCryptFileName, char *pDecryptFileName, unsigned char *pKey, unsigned char *pIV)
{
FILE          *pCryptFile;
FILE          *pDecryptFile;
unsigned char  cryptBuf[BUF_SIZE];
unsigned char  clearBuf[BUF_SIZE];
int            nbByteRead;
int            lenCrypt  =0;
int            lenPadding = 0;

EVP_CIPHER_CTX *ctx;

    ctx = EVP_CIPHER_CTX_new();
    EVP_CIPHER_CTX_init (ctx);
    EVP_DecryptInit (ctx, EVP_aes_128_ecb(), pKey, pIV);

    memset (&clearBuf[0], 0, sizeof(clearBuf));
    memset (&cryptBuf[0], 0, sizeof(cryptBuf));



    pCryptFile   = fopen(pCryptFileName,   "rb");
    pDecryptFile = fopen(pDecryptFileName, "wb");
    nbByteRead   = fread(&cryptBuf[0], 1, sizeof(cryptBuf), pCryptFile);

    EVP_DecryptUpdate (ctx, &clearBuf[0], &lenCrypt, &cryptBuf[0], nbByteRead);
    fwrite (&clearBuf[0], 1, lenCrypt, pDecryptFile);
    EVP_DecryptFinal (ctx, &clearBuf[lenCrypt], &lenPadding);
    fwrite (&clearBuf[lenCrypt], 1, lenPadding, pDecryptFile);





    EVP_CIPHER_CTX_cleanup (ctx);
    EVP_CIPHER_CTX_free(ctx);
    fclose (pDecryptFile);
    fclose (pCryptFile);
    return (0);
}



