#!/usr/bin/python
from Crypto.Cipher import DES   #/usr/lib64/python2.7/site-packages/Crypto/Cipher

#clear text
p2 =  bytearray([0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x01])
iv =  bytearray([0x11, 0xff, 0x03, 0xed, 0x2d, 0x3f, 0xce, 0xb4])
key = bytearray([0x01, 0x10, 0x2b, 0x37, 0x2d, 0xa4, 0x42, 0x23])

# encrypt
cipher=DES.new(str(key), DES.MODE_CBC, str(iv))
c2=bytearray(cipher.encrypt(str(p2)))


print "p2 =", ":".join("{:02x}".format(ord(c)) for c in str(p2))
print "iv =", ":".join("{:02x}".format(ord(c)) for c in str(iv))
print "c2 =", ":".join("{:02x}".format(ord(c)) for c in str(c2))


#padding oracle attack
c1 =  bytearray([0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0])
d21=  bytearray([0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0])

for j in range (0, 8):
    for i in range (0,256):
        c1[7-j] = i
        cipher=DES.new(str(key), DES.MODE_CBC, str(c1))
        d2 = bytearray(cipher.decrypt(str(c2)))
        if d2[7-j] == (j+1):
            print "byte:", j, " d2 =", ":".join("{:02x}".format(ord(c)) for c in str(d2))
            d21[7-j] = d2 [7-j] ^ c1[7-j]

    for k in range (0,8):
        c1 [k] = d21[k] ^ (j+2)

# print the plain text
for j in range(0,8):
    print hex(iv[j] ^ d21[j]) 



