///www.openssl.org/docs/crypto/EVP_DigestInit.html
//# gcc -Wall -Wextra -o sha sha.c -lcrypto

	
#include <openssl/blowfish.h>
#include <openssl/evp.h>
#include <stdio.h>
#include <string.h>


#define SHA1_SIZE 22
#define SIZE      1024

static int hash (void);


int main (int argc, char *argv[])
{

    hash ();
    return (0);
}






static int hash (void)
{
EVP_MD_CTX    *ctx;
unsigned char buffer1[32];
unsigned char buffer2[32];

unsigned char hash[32];
unsigned int  hashLength;
unsigned int  i;
  
    hashLength = 0;
    memset (&buffer1[0], 0xA, sizeof(buffer1));
    memset (&buffer2[0], 0xb, sizeof(buffer2));
    memset (&hash[0],    0x0, sizeof(hash));


    ctx=EVP_MD_CTX_new();
    EVP_DigestInit(ctx, EVP_sha256());
    
    //EVP_DigestUpdate(ctx, &buffer1[0], sizeof(buffer1));
    EVP_DigestUpdate(ctx, &buffer1[0], 10);
    EVP_DigestUpdate(ctx, &buffer1[10], 22);


    EVP_DigestFinal(ctx, &hash[0], &hashLength);
    EVP_MD_CTX_free(ctx);

    printf ("Hash value = ");
    for (i=0; i<hashLength; i++)
        printf ("%.2x", hash[i]);
    printf ("\n");

    return (0);
}

