// gcc -Wall -o client_ipv4_ipv6 client_ipv4_ipv6.c
// tcpdump -i lo udp -s0  -w t.pcap

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h> 

#define NB_LOOP     3
static int socketIPv4(void);
static int socketIPv6(void);

int main()
{

    socketIPv4();
    //socketIPv6();
    return 0;
}


static int socketIPv4(void)
{
struct sockaddr_in  sin;
int		    s;
char		    buffer1[32];
char                buffer2[32];
int                 i;
    

    s = socket (AF_INET, SOCK_DGRAM, 0);
    memset (&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    //sin4.sin_addr.s_addr = inet_addr("127.0.0.1");
    //inet_pton(AF_INET, "127.0.0.1", &sin.sin_addr);
    inet_pton(AF_INET, "160.98.30.120", &sin.sin_addr);
    sin.sin_port = htons(1234);
    connect (s, (struct sockaddr *) &sin, sizeof(sin));

    printf("start\n");
    
    for (i=0; i<NB_LOOP; i++)
    {
        memset (&buffer1[0], 0, sizeof(buffer1));
        memset (&buffer2[0], 0, sizeof(buffer2));
        printf ("Enter text: \n");
        scanf ("%s", &buffer1[0]);
        write (s, &buffer1[0], sizeof(buffer1));
        read  (s, &buffer2[0], sizeof(buffer2));
        printf ("client: %s\n", &buffer2[0]);
    }
    close (s);
    printf("stop\n");

    return 0;
}



static int socketIPv6(void)
{
struct sockaddr_in6 sin;
int		    s;
char		    buffer1[32];
char                buffer2[32];
int                 i;
    
	
    
    s = socket (AF_INET6, SOCK_DGRAM, 0);
    memset (&sin, 0, sizeof(sin));
    sin.sin6_family = AF_INET6;
    sin.sin6_port   = htons(1234);
    inet_pton(AF_INET6, "::1", &sin.sin6_addr);
    connect (s, (struct sockaddr *) &sin, sizeof(sin));
    

    printf("start\n");
    
    for (i=0; i<NB_LOOP; i++)
    {
        memset (&buffer1[0], 0, sizeof(buffer1));
        memset (&buffer2[0], 0, sizeof(buffer2));
        printf ("Enter text: \n");
        scanf ("%s", &buffer1[0]);
        write (s, &buffer1[0], sizeof(buffer1));
        read  (s, &buffer2[0], sizeof(buffer2));
        printf ("client: %s\n", &buffer2[0]);
    }
    close (s);
    printf("stop\n");

    return 0;
}

