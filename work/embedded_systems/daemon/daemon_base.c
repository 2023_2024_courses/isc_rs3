#include <stdio.h>

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <syslog.h>

static void setDaemon        (void);
static void catchSignal      (int signal);
static void createCatchSignal(void);

static FILE *pFile;


int main (int argc, char **argv)
{
int   i;

    createCatchSignal ();


    setDaemon();
    
    pFile = fopen ("t.txt", "w");
    for (i=0;;i++)
    {
        printf ("coucou\n");
        fprintf(pFile,"increment=%d\n",i);
        fflush (pFile);
        sleep(1);
    }
    exit(EXIT_SUCCESS);
}

static void  setDaemon(void)
{ 
pid_t pid, sid;

    
    
    pid = fork();                               // Fork off the parent process
    
    if (pid < 0) {
       exit(EXIT_FAILURE);
    }
    
    if (pid > 0) {   
       exit(EXIT_SUCCESS);  
    }
  
    sid = setsid();  
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }
   
    close(STDIN_FILENO); 
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
}


static void createCatchSignal(void)
{
struct sigaction act;

    memset (&act, 0, sizeof (act));
    act.sa_handler = catchSignal;
    sigaction (SIGTERM, &act, 0);  // kill PID
}


static void catchSignal (int signal)
{
    fclose (pFile);
    syslog(LOG_INFO, "Stop OK");
    exit(EXIT_SUCCESS);
}

