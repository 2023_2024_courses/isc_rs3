#define QUEUE_THR3     3

#define MSG_TYPE       2


#define SENDER_THR1    1
#define SENDER_THR2    2
#define SENDER_THR3    3

#define MAX_VALUE     16

struct MSG_THR1 
{
   unsigned int   value1;
   unsigned char  value2;
};

struct MSG_THR2 
{
   unsigned char  buffer[MAX_VALUE];
};


struct HEADER
{
   unsigned short sender;
   unsigned char  buffer[MAX_VALUE];
};


struct MSG
{
   long int      type;
   struct HEADER header;
};   





