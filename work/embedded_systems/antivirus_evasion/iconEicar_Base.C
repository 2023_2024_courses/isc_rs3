//gcc -Wall -g -o iconEicar iconEicar.C -lstdc++

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>


typedef struct
{
	uint16_t 	ImageFileType ; 	/* Always 4D42h (''BM'')		*/
	uint32_t 	FileSize ;	        /* Physical file size in bytes 		*/
	uint16_t	Reserved1 ;
	uint16_t	Reserved2 ;
	uint32_t	ImageDataOffset ;	/* Start of image data offset in byte 	*/
}__attribute__((__packed__)) BITMAP_HEADER1 ;

typedef struct
{
	uint16_t 	HeaderSize ;		/* Size of this header 			*/
	uint16_t	Reserved1;
	uint16_t	ImageWidth ;		/* Image width in pixel			*/
	uint16_t	Reserved2;
	uint16_t	ImageHeight ;		/* Image height in pixel			*/
	uint16_t	Reserved3;
	uint16_t	NumberOfImagePlanes ;/* always 1				*/
	uint16_t	BitPerPixel ;		/* 1, 4, 8, 24				*/
	uint32_t	CompressionMethod ;	/* 0, 1, 2				*/
	uint32_t	SizeOfBitmap ;		/* Size of the bitmap in bytes		*/
	uint32_t	HorzResolution ;	/* Horiz. Resolution in pixel per meter	*/
	uint32_t	VertResolution ;	/* Vert. Resolution in pixel per meter	*/
	uint32_t	NumColorsUsed ;	    /* Number of the colors in the bitmap	*/
	uint32_t	NumSignificantColors ;	/* Number of important colors in palette	*/
}__attribute__((__packed__)) 	BITMAP_HEADER2 ;

typedef struct
{
	uint8_t 	Blue ;
	uint8_t		Green ;
	uint8_t		Red ;
}__attribute__((__packed__))	RGB ;


static void addEicarVirus (const char *pSrc, const char *pDst, const char *pEicarVirus);



int main(void)
{
    addEicarVirus ("icon.bmp", "icon2.bmp", "eicar.com");
    return 0;
}



static void addEicarVirus (const char *pSrc, const char *pDst, const char *pEicarVirus)
{
FILE          *p1;
FILE          *p2;
int            nb_byte_line;
RGB           *p_buffer;

BITMAP_HEADER1 h1;
BITMAP_HEADER2 h2;


    /* Open the files */
    p1 = fopen(pSrc, "rb");
    p2 = fopen(pDst, "wb");


    // Read the two headers
    fread  (&h1, 1, sizeof(h1), p1);
    //h1.ImageFileType
    fwrite (&h1, 1, sizeof(h1), p2);

    // nb bytes for one line 
    nb_byte_line = (h2.ImageWidth * sizeof(RGB)) + (h2.ImageWidth % 4);

    // dynamic allocation
    p_buffer = (RGB*)new unsigned char[nb_byte_line];

    // free dynamic allocation
    delete [] p_buffer;
    
    fclose(p1);

}


