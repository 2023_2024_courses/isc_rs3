// create named pipe: mkfifo -m 0666 /tmp/myFifo

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>






#define NAMED_PIPE "/tmp/myFifo"
#define PIPE_BUF 4096

int main(void)
{
int   i;
int   nbBytes;
int   pipe_fd;
char *p;

    p=malloc (PIPE_BUF);
    memset (p, 'A', PIPE_BUF);
    printf ("PIPE_BUF=%d\n", PIPE_BUF);

    pipe_fd = open(NAMED_PIPE, O_WRONLY);

    for (; ; )
    {
        nbBytes = write (pipe_fd, p, PIPE_BUF);
        printf ("nbBytes=%d\n", nbBytes);
        sleep(1);
    }

    close (pipe_fd);
    free(p);

}

