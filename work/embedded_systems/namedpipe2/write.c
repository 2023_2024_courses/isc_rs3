#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <pthread.h>








#define NAMED_PIPE "/tmp/myFifo"
#define PIPE_BUF 16


// Prototype functions
static void catchSignal      (int signal);
static void createCatchSignal(void);

static int   pipe_fd;


int main(void)
{
int   i;
int   nbBytes;
char  buf[16];

    createCatchSignal ();


    memset (&buf[0], 'A', sizeof(buf));

    if (access (NAMED_PIPE, F_OK) == -1)
        mkfifo (NAMED_PIPE, 0777);


    for (i=10; i; i--)
    {
        pipe_fd = open(NAMED_PIPE, O_WRONLY);
        nbBytes = write (pipe_fd, &buf[0], sizeof(buf));
        close (pipe_fd);
        printf ("nbBytes=%d\n", nbBytes);
        sleep(1);
    }

}

static void createCatchSignal(void)
{
struct sigaction act;

    act.sa_handler = catchSignal;
    sigemptyset (&act.sa_mask);
    act.sa_flags   = 0;
    sigaction (SIGINT,  &act, 0);
    sigaction (SIGTSTP, &act, 0);
    sigaction (SIGTERM, &act, 0);
    sigaction (SIGABRT, &act, 0);
}


static void catchSignal (int signal)
{
    printf ("signal = %d\n", signal);
    //close (pipe_fd);
    //unlink(NAMED_PIPE);
    pthread_exit (NULL);

}





