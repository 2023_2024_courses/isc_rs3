#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

#include <unistd.h>



#define K1     "0"
#define K2     "2"
#define K3     "3"

#define LED0   "10"
#define LED1  "362"

#define GPIO_EXPORT    "/sys/class/gpio/export"
#define GPIO_UNEXPORT  "/sys/class/gpio/unexport"

#define GPIO_PATH      "/sys/class/gpio/gpio"
#define GPIO_DIRECTION "/direction"
#define GPIO_VALUE     "/value"
#define IN             "in"
#define OUT            "out"


static int           activateGPIO    (char *pGPIO, char *pDir);
static int           deactivateGPIO  (char *pGPIO);
static unsigned char readGPIO        (char *p);
static int           writeGPIO       (char *p, unsigned char val);








int main (void)
{
int i;

    activateGPIO(K1, IN);
    activateGPIO(K2, IN);
    activateGPIO(K3, IN);
    activateGPIO(LED0, OUT);
    activateGPIO(LED1, OUT);
 
    

    for (i=0; i<10;i++) {

        printf ("K1=%d\n",readGPIO (K1));
        printf ("K2=%d\n",readGPIO (K2));
        printf ("K3=%d\n",readGPIO (K3));
        printf ("LED0=%d\n",readGPIO (LED0));
        printf ("LED1=%d\n",readGPIO (LED1));

        sleep(1);
    }

    for (i=0; i<10;i++) {

        printf ("K1=%d\n",writeGPIO (K1, 1));
        printf ("K2=%d\n",writeGPIO (K2, 1));
        printf ("K3=%d\n",writeGPIO (K3, 1));
        printf ("LED0=%d\n",writeGPIO (LED0, (unsigned char)(i%2)));
        printf ("LED1=%d\n",writeGPIO (LED1, (unsigned char)(i%2)));

        sleep(1);
    }




    deactivateGPIO(K1);
    deactivateGPIO(K2);
    deactivateGPIO(K3);
    deactivateGPIO(LED0);
    deactivateGPIO(LED1);


}

static unsigned char readGPIO (char *p) {
int           fd;
int           val;
int           nbByte;
unsigned char input;
char          file [128];

    memset (file, 0, sizeof(file));
    strcat (file, GPIO_PATH);
    strcat (file, p);
    strcat (file, GPIO_VALUE); // file = "/sys/class/gpio/gpioX/value"

    val=0;
    fd = open(file,O_RDONLY);
    nbByte=pread (fd, &val, sizeof(val), 0);
    close (fd);
    input = (unsigned char) val - '0';
    printf ("nbByte=%d, val=%d, input=%d\n", nbByte, val, input);
    return (input);


}


static int  writeGPIO (char *p, unsigned char val) {
int           fd;
int           nbByte;
char          file [128];

    memset (file, 0, sizeof(file));
    strcat (file, GPIO_PATH);
    strcat (file, p);
    strcat (file, GPIO_VALUE); // file = "/sys/class/gpio/gpioX/value"

    val=val+'0';
    nbByte = 0;
    fd = open(file,O_WRONLY);
    if (fd) {
        nbByte=pwrite (fd, &val, sizeof(val), 0);
        close (fd);
    }
    printf ("nbByte=%d\n", nbByte);
    return (nbByte);


}




static int activateGPIO(char *pGPIO, char *pDir) {
int   fd;
char  file [128];


    // activate input
    fd = open(GPIO_EXPORT,O_WRONLY); //  "/sys/class/gpio/export"
    write (fd, pGPIO, strlen(pGPIO));
    close (fd);

    // indicate direction = in
    memset (file, 0, sizeof(file));
    strcat (file, GPIO_PATH);
    strcat (file, pGPIO);
    strcat (file, GPIO_DIRECTION);  //  file = "/sys/class/gpio/gpioX/direction"

    printf ("%s\n", file);

    fd = open(file,O_RDWR);
    write (fd, pDir, strlen(pDir));
    close (fd);

    return (0);
}


static int deactivateGPIO(char *pGPIO) {
int  fd;

    // activate input
    fd = open(GPIO_UNEXPORT,O_WRONLY); //  "/sys/class/gpio/unexport"
    write (fd, pGPIO, strlen(pGPIO));
    close (fd);
    return (0);
}





