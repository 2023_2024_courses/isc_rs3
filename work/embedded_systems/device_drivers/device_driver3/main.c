#include <stdio.h>
#include <string.h>

#include <fcntl.h>
#include <unistd.h>


int main()
{
int userVal1;
int userVal2;
int fd;
int nbBytes;
   
   fd = open ("/dev/drv3", O_RDWR);
   userVal1=10;
   nbBytes = write(fd, &userVal1, sizeof(userVal1));
   printf ("write, nbBytes=%d\n", nbBytes);
   nbBytes=read(fd, &userVal2, sizeof(userVal2));
   printf ("read, nbBytes=%d, userVal2=%d\n", nbBytes, userVal2);

   close (fd);

   fd = open ("/dev/drv3_1", O_RDWR);
   userVal1=10;
   nbBytes = write(fd, &userVal1, sizeof(userVal1));
   printf ("write, nbBytes=%d\n", nbBytes);
   nbBytes=read(fd, &userVal2, sizeof(userVal2));
   printf ("read, nbBytes=%d, userVal2=%d\n", nbBytes, userVal2);

   close (fd);


   return 0;
}
