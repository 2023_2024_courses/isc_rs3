#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>



extern sem_t semaphore2;
extern sem_t semaphore3;



static void            cleaner  (void *p);


void *thr_3 (void *arg)
{

    pthread_setcancelstate (PTHREAD_CANCEL_ENABLE, NULL);
    pthread_setcanceltype  (PTHREAD_CANCEL_DEFERRED, NULL);
    pthread_cleanup_push   (cleaner, NULL);


    for (; ; )
    {
        sem_wait (&semaphore2);
        printf ("Thread 3\n");
        sem_post (&semaphore3);

    }




    pthread_cleanup_pop(0);
    pthread_exit (NULL);
}

static void cleaner (void *p)
{
    printf ("thr_3: Thread end\n");
}



