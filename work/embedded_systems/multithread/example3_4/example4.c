//  gcc -Wall -Wextra -D_REENTRANT -o thread2 thread2.c -lpthread


#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>


void *thr_funct1 (void *arg);
void *thr_funct2 (void *arg);


int main (void)
{
pthread_t thread_ID1, thread_ID2;
void     *returnMessage;
char     *p_message;


    p_message = "Thread 1";
    pthread_create (&thread_ID1, NULL, thr_funct1, (void*)p_message);
    printf ("Main Thread_ID 1 = %x\n", (int)thread_ID1);


    p_message = "Thread 2";
    pthread_create (&thread_ID2, NULL, thr_funct2, (void*)p_message);
    printf ("Main Thread_ID 2 = %x\n", (int)thread_ID2);

    printf ("Main Wait\n");

    pthread_join (thread_ID1, &returnMessage);
    printf ("Main Message from the thread 1 %s\n", (char*)returnMessage);
    pthread_join (thread_ID2, &returnMessage);

    printf ("Main Message from the thread 2 %s\n", (char*)returnMessage);
    exit (EXIT_SUCCESS);
}

void* thr_funct1 (void *arg)
{
    printf ("1 The thread 1 begins, the argument is: %s\n", (char*)arg);
    sleep (2);
    pthread_exit ("Thread1 end\n");
}



void *thr_funct2 (void *arg)
{
    printf ("2 The thread 2 begins, the argument is: %s\n", (char*) arg);
    sleep (4);
    pthread_exit ("Thread2 end\n");
}

