#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>


static void            cleaner  (void *p);

void *thr_1 (void *arg) {
    pthread_setcancelstate (PTHREAD_CANCEL_ENABLE, NULL);
    pthread_setcanceltype  (PTHREAD_CANCEL_DEFERRED, NULL);
    pthread_cleanup_push   (cleaner, NULL);

    for (;;) {
        printf ("Thread 1\n");
        sleep (1);
    }
    pthread_cleanup_pop(0);
    pthread_exit (NULL);
}

static void cleaner (void *p) {
    printf ("thr_1: Thread end\n");
}



