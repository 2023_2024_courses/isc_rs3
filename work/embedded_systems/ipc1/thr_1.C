#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>


#include "my_message.h"


extern int   id_queue_thr2;


static void  cleaner  (void *p);

void *thr_1 (void *arg)
{
struct MSG   msg;
 

    pthread_setcancelstate (PTHREAD_CANCEL_ENABLE, NULL);
    pthread_setcanceltype  (PTHREAD_CANCEL_DEFERRED, NULL);
    pthread_cleanup_push   (cleaner, NULL);


    for (;;)
    {
        memset (&msg, 0, sizeof(msg));
        msg.type          = MSG_TYPE;
        msg.header.sender = SENDER_THR1;
        msg.header.val1   = 10;
        msg.header.val2   = 20;
        msgsnd (id_queue_thr2, &msg, sizeof (msg.header), 0);
        sleep (3);
    }



    pthread_cleanup_pop(0);
    pthread_exit (NULL);
}

static void cleaner (void *p)
{

    printf ("thr_1: Thread end\n");
}



