#!/bin/sh
BOARD_DIR="$(dirname $0)"
BUILDROOT_DIR="/buildroot"

#install -m 0644 -D $BOARD_DIR/extlinux.conf $BINARIES_DIR/extlinux/extlinux.conf
mkimage -C none -A arm64 -T script -d $BOARD_DIR/boot.cmd $BUILDROOT_DIR/output/images/boot.scr

# Create Image.itb
cp $BOARD_DIR/kernel_fdt.its $BUILDROOT_DIR/output/images
mkimage -f $BUILDROOT_DIR/output/images/kernel_fdt.its -E $BUILDROOT_DIR/output/images/Image.itb 

# create boot.ext4 file, this file is used by genimage
BOOT_EXT4_DIR=$BUILDROOT_DIR/boot_ext4_dir
echo $BOOT_EXT4_DIR
mkdir $BOOT_EXT4_DIR
dd if=/dev/zero of=$BOOT_EXT4_DIR/boot.ext4 bs=1024 count=65536
mkfs.ext4 -L boot $BOOT_EXT4_DIR/boot.ext4
mount -o loop $BOOT_EXT4_DIR/boot.ext4 /mnt
cp output/images/boot.scr /mnt
cp output/images/Image.itb /mnt
cp output/images/sun50i-h5-nanopi-neo-plus2.dtb /mnt
umount /mnt
cp $BOOT_EXT4_DIR/boot.ext4 output/images/.
rm -rf $BOOT_EXT4_DIR

